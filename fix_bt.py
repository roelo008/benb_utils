import numbers


def fix_bt(code_in, as_mnp=False, verbose=False, strict=True, pass_missing_N=False):
    """
    Parser for Beheertype codes to repair abbreviated codes and/or MNP extension
    eg. 02.01     --> N02.01 if as_mnp==False and pass_missing_N=True
        02.01     --> N01.01.00 if as_mnp==True and pass_missing_N=True
        N05.01    --> N05.01  if as_mnp==False
        N05.01    --> N05.01.00  if as_mnp==True
        N04.02.01 --> N04.02.01 if as_mnp==False
        N04.02.01 --> N04.02.01 if as_mnp==True
    BT Code is as follows <N><AA>.<BB>.<CC>
    N = letter, may be missing
    A = two digits indicating top level beheertype "TOP"
    B = two digits indicating sub level beheertype "SUB"
    C = two digits indicating neergeschaald beheertype if > 00. Else "00" as used in MNP param notation  "NEER"
    N04.02.00 is equivalent to N04.02
    :param code_in: beheertypecode in any format
    :param as_mnp: return MNP style beheertypecode, default=False
    :param verbose: verbose feedback
    :param strict: raise error if bt code is unexpected
    :param pass_missing_N: allow missing Capital letter at start of code, assuming N
    :return: beheertypecode
    """

 

    if isinstance(code_in, numbers.Number):
        code_in = str(code_in)

    # Identify parts of code
    parts = code_in.split(".")
    if len(parts) == 1:
        if strict:
            raise AssertionError("1. Unexpected BT code: {}".format(code_in))
        else:
            return None
    elif len(parts) == 2:
        top, sub = parts
        neer = None
    elif len(parts) == 3:
        top, sub, neer = parts
    else:
        if strict:
            raise AssertionError("2. Unexpected BT code: {}".format(code_in))
        else:
            return None

    if verbose:
        print('from {}'.format(code_in), end=' ')


    # Verify TOP and add "N" is required
    if top[0].upper() in ['N', 'W', 'S', 'T', 'A', 'L']:
        pass
    else:
        if pass_missing_N: 
            print('warning: Capital letter missing from code. Defaulting to Natuurtype, interpreting "{0}" as "{1}"'.format(top, 'N{}'.format(top)))
            top = 'N{}'.format(top)
            
    assert top[0].upper() in ['N', 'W', 'S', 'T', 'A', 'L'], "3. Unexpected BT Code: {}".format(code_in)
    if top[0].islower():
        top = top.capitalize()
    assert top[1].isdigit(), '3. Unexpected BT code: {}'.format(code_in)
    assert top[2].isdigit(), '3. Unexpected BT code: {}'.format(code_in)
    assert len(top) == 3, "3. Unexpected BT Code: {}".format(code_in)

    # Verify SUB
    assert sub.isdigit and len(sub) == 2, "4. Unexpected BT Code: {}".format(code_in)

    # Verify NEER and check if it is neergeschaald or not
    keep_neer = False
    if neer:
        assert neer.isdigit and len(neer) == 2, "5. Unexpected BT Code: {}".format(code_in)
        if int(neer) > 0:
            keep_neer = True

    # Construct output
    head = "{}.{}".format(top, sub)
    if keep_neer:
        output = "{}.{}".format(head, neer)
    elif as_mnp:
        output = "{}.00".format(head)
    else:
        output = head
    if verbose:
        print('to {}'.format(output))
    return output