"""
Find  value of Abiotic Condition (AC) to accomodate as many species as possible based on species response to the AC
Hans Roelofsen, 24-01-2022
"""


import os
import sys
import pathlib
import datetime
import numpy as np
import pandas as pd
import rasterio as rio

from Classes.Species import IndividualSpecies as Species
import dkq
import read_sources as src

# Append one level up to sys path and import mrt module from there.
# See: https://git.wur.nl/roelo008/mrt
# fp = r'C:\apps\proj_code\benb_utils\ab_opt.py'
fp = pathlib.Path(__file__)
sys.path.append(str(pathlib.Path(fp).parents[1]))
from mrt.sample import mrt_helpers as mrt


class AbioticOptimum:
    """
    Class for holding and calculating abiotic optima for a Beheertype
    Hans Roelofsen, WEnR, mrt 2022
    """

    def __init__(self):
        """
        Initiate
        """

        # Set ranges of abiotic values
        self.ph_range = [i/10 for i in range(0, 140, 1)]
        self.gvg_range = [i for i in range(-50, 150, 5)]
        self.ndep_range = [i for i in range(410, 3680, 5)]

        # Get draagkracht class instance
        self.dk = dkq.DraagKracht()

        # Paths to default files with species response towards abiotic conditions
        self.gvg_response_src = r'w:\PROJECTS\QMAR\MNP-SNL-ParameterSet\Parameters_v05_2021_04_08\04_MNP_versie4_par_response_GVG.csv'
        self.ndep_response_src = r'w:\PROJECTS\QMAR\MNP-SNL-ParameterSet\Parameters_v05_2021_04_08\05_MNP_versie4_par_response_Ndep.csv'
        self.ph_response_src = r'w:\PROJECTS\QMAR\MNP-SNL-ParameterSet\Parameters_v05_2021_04_08\06_MNP_versie4_par_response_PH.csv'

        self.gvg_response = pd.read_csv(self.gvg_response_src, index_col='Species_code')
        self.ndep_response = pd.read_csv(self.ndep_response_src, index_col='Species_code')
        self.ph_response = pd.read_csv(self.ph_response_src, index_col='Species_code')

    def get_abiotic_limits(self, sp_code, abiotic):
        """
        Return abiotic limits for a species
        :param sp_code: species code as formatted in MNP
        :param abiotic: {ph, ndep, ph}
        """

        # TODO: dit past misschien beter bij Classes.Species.IndividualSpecies

        tab = getattr(self, '{}_response'.format(abiotic))
        return tab.loc[sp_code]

    def optimum_for_bt(self, bt, abiotic_condition, sp_list, of='tuple', verbose=False):
        """
        return optimum value of a abiotic condition given a Beheertype and species list
        :param bt: Beheertype
        :param abiotic_condition: {gvg, ph, ndep}
        :param sp_list: {146, 281}
        :param of: output format
        :return: float
        """

        # Get species list corresponding to this beheertype
        species_lst = [x for x, _ in self.dk.query4bt(bt, of='dict', sp_sel=str(sp_list)).items()]
        assert species_lst, "no species found for {}".format(bt)

        # Empty dataframe for results
        response_df = pd.DataFrame(data={'response_sum': 0, 'n_species': 0},
                                   index=getattr(self, '{}_range'.format(abiotic_condition)))

        # Loop over species in ht especies list
        for c in species_lst:
            sp = Species(c, brief=True)
            # species_abiotic_limits = self.get_abiotic_limits(sp.code, abiotic_condition)

            # Loop over each abiotic value in the provided range
            for i in response_df.index:

                # Species response to this abiotic value {0, 0.5, 1}
                species_response = sp.response_to_abiotic(abiotic_condition, i)

                # Add tot summed species response
                response_df.loc[i, 'response_sum'] = response_df.loc[i, 'response_sum'] + species_response

                # Add 1 to number of species with response > 0
                if species_response > 0:
                    response_df.loc[i, 'n_species'] = response_df.loc[i, 'n_species'] + 1

        # The optimal abiotic condition is where:
        # first the response_sum is highest
        #   then the n_species is highest
        #      then the lowest abiotic value
        # Note: dit is zoals gesuggereerd door Marlies Sanders, 7 mrt 2022
        top_score = response_df.response_sum.max()
        abiotic_optimum = response_df.loc[response_df.response_sum == top_score, 'n_species'].idxmax()
        n_with_optimum = response_df.loc[response_df.response_sum ==top_score].shape[0]

        # Score is the summed species response PLUS number of species with response > 0
        # Dit is een alternatieve methode.
        # response_df['score'] = response_df.sum(axis=1)
        # Determine top score and corresponding abiotic condition value
        # top_score = response_df.score.max()
        # abiotic_optimum = response_df.score.idxmax()  # First occurence of maximum
        # n_with_optimum = response_df.loc[response_df.score == top_score].shape[0]

        msg = '{0} optimum for {1} is {2}'.format(abiotic_condition, bt, abiotic_optimum)
        if verbose:
            print(msg)

        if of == 'df':
            response_df.to_clipboard(index=True)

        return {'tuple': (len(species_lst), top_score, n_with_optimum, abiotic_optimum),
                'df': response_df,
                'dict': {bt: abiotic_optimum},
                'single': abiotic_optimum,
                'csv': f'{bt},{abiotic_condition},{abiotic_optimum}',
                'str': msg}[of]

    def remap_from_bts(self, abiotic, sp_list, bt_raster, bt_vat_file, outdir, outname, mapping='from_species',
                       **kwargs):
        """
        Remap a Beheertypen map to the abiotic optimum corresponding to each beheertype
        :param abiotic: {gvg, ph, ndep}
        :param sp_list: which species list?
        :param bt_raster: path to template beheertypen raster
        :param bt_vat_file: path to corresponding raster vat (*.tif.vat.dbf OR *.csv)
        :param out_dir: output directory
        :param out_name: output name
        :param mapping: mapping bteen beheertype and optima {from_species, from_file}
        :kwargs: mapping file: path to table file if mapping == from_file
                 key: key column
                 val: value column
        :return: raster file on disk
        """

        abiotic = abiotic.lower()

        # Open template beheertypen raster
        bt = rio.open(bt_raster)
        bt_arr = bt.read(1)

        # Build output profile
        prof = mrt.mnp_abiotiek_raster_profile()
        assert bt.shape == (prof['height'], prof['width'])
        assert bt.transform == prof['transform']

        # Read mapping between beheertypen and abiotic optima
        bt_vat = pd.DataFrame.from_dict(src.mapping_from_file(bt_vat_file, key_col='Value', value_col='Descriptio'),
                                        orient='index', columns=['Description'])
        bt_vat['code'] = [x[0] for x in bt_vat.Description.str.split(' ')]
        bt_vat['optimum'] = np.zeros(bt_vat.shape[0]).astype(prof['dtype'])
        bt_vat['Value'] = bt_vat.index

        # Gather abiotic optimum for beheertype
        if mapping == 'from_file':
            print('  reading {0} optima from file: {1}'.format(abiotic, kwargs.get('mapping_file')))
            bt2opt = src.mapping_from_file(src=kwargs.get('mapping_file'),
                                           key_col=kwargs.get('key'),
                                           value_col=kwargs.get('val'),
                                           sep=kwargs.get('sep'))
            origin = 'read from file: {0}'.format(kwargs.get('mapping_file'))
        else:
            origin = 'optimisation for {0} species list, based on {1}'.format(sp_list,
                                                                              getattr(self, '{}_response_src'.format(abiotic)))

        for code in bt_vat.code.values:
            try:
                optimum = {'from_file': lambda x: bt2opt[x],
                           'from_species': lambda x: self.optimum_for_bt(x, abiotic, sp_list, 'single',
                                                                         verbose=True)}[mapping](code)
            except (AssertionError, KeyError) as e:
                print(e)
                continue
            bt_vat.loc[bt_vat.code == code, 'optimum'] = optimum


        # Set non-compatible beheertypen and otherwise to Nodata
        bt_vat.optimum.fillna(prof['nodata'], inplace=True)

        # Apply mapping between BT map pixel value and optimum
        d = dict(zip(bt_vat['Value'], bt_vat['optimum']))
        d[bt.nodata] = prof['nodata']
        out = mrt.vec_translate(bt_arr, d).astype(prof['dtype'])

        # Write to file
        destination = os.path.join(outdir, outname)
        with rio.open(destination, 'w', **prof) as dest:
            dest.update_tags(creation_date=datetime.datetime.now().strftime('%d-%b-%Y_%H:%M:%S'),
                             created_by=os.environ.get('USERNAME'),
                             beheertypen_src_file=bt_raster,
                             content='{0} based on {1}'.format(abiotic, origin),
                             created_with=os.path.basename(__file__))
            dest.update_tags(**dict(zip(bt_vat.Description, bt_vat.optimum)))
            dest.write(out, 1)
        if os.path.isfile(destination):
            print('...succes, see {}'.format(destination))


if __name__ == '__main__':

    def query4bt(**kwargs):
        foo = AbioticOptimum()
        out = foo.optimum_for_bt(bt=kwargs.get('bt'), abiotic_condition=kwargs.get('abiotic'),
                                 sp_list=kwargs.get('sp_list'), of=kwargs.get('of'))
        print(out)

    def remap_bt(**kwargs):
        foo = AbioticOptimum()
        kwargs['mapping'] = 'from_file' if all(kwargs.get('fromfile')) else 'from_species'
        kwargs['mapping_file'], kwargs['key'], kwargs['val'], kwargs['sep'] = kwargs.get('fromfile')
        try:
            foo.remap_from_bts(**kwargs)
        except (KeyError, AssertionError) as e:
            print(e)
            sys.exit(0)

    def query4sp(**kwargs):
        foo = AbioticOptimum()
        sp = Species(kwargs.get('species'))
        abiotic_limits = foo.get_abiotic_limits(sp.code, kwargs.get('abiotic'))
        d = dict(zip(['L20', 'L80', 'H80', 'H20'], abiotic_limits))
        df = pd.DataFrame.from_dict(d, orient='index', columns=[kwargs.get('abiotic')])
        df.T.to_clipboard(sep='\t')
        print({'dict': d,
               'csv': df.to_csv(sep='\t')}[kwargs.get('of')])

    def species_response(**kwargs):
        """
        Get a species response to an abiotic value
        :param kwargs:
        :return:
        """
        foo = AbioticOptimum()
        sp = Species(kwargs.get('species'))
        # abiotic_limits = foo.get_abiotic_limits(sp.code, kwargs.get('abiotic'))
        response = sp.response_to_abiotic(kwargs.get('abiotic'), kwargs.get('val'))

        print({'simpleton': response,
               'verbose': '{0} ({1}) response to {2}={3} is: {4}'.format(sp.local, sp.scientific,
                                                                        kwargs.get('abiotic'), kwargs.get('val'),
                                                                        response)}[kwargs.get('of')])


    def mixed_species_response(**kwargs):
        """
        Determine overall species response to 1 or more abiotic conditions
        :param kwargs:
        :return:
        """
        foo = AbioticOptimum()
        sp = Species(kwargs.get('species'))
        abiotics = kwargs.get('abiotics')

        assert len(abiotics) % 2 == 0
        d = dict(zip([i for i in abiotics if (abiotics.index(i) % 2 == 0 and i in [])],
                     [i for i in abiotics if abiotics.index(i) % 2 == 1]))
        responses = []
        for k, v in d.items():
            abiotic_limits = foo.get_abiotic_limits(sp.code, k)
            responses.append(sp.response_to_abiotic(abiotic_limits, v))
        return np.prod(responses)



    import argparse
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(dest='subparser_name')

    # Subparser for remapping a Beheertypen map to Abiotic Optima
    parser_a = subparsers.add_parser('remapBT', help='Remap a Beheertypen map to abiotic optima')
    parser_a.add_argument('abiotic', help='abiotic condition', type=str, choices=['gvg', 'ph', 'ndep'])
    parser_a.add_argument('bt_raster', help='beheertypen raster @ 25m', type=str)
    parser_a.add_argument('bt_vat_file', help='corresponding vat', type=str)
    parser_a.add_argument('outdir', help='output dir', type=str)
    parser_a.add_argument('outname', help='output name', type=str)
    parser_a.add_argument('--sp_list', help='which species list', type=int, choices=[146, 281, 468], default=281)
    parser_a.add_argument('--fromfile', help='src_file key_col val_col seperator when reading mapping from file',
                          nargs=4, default=[None, None, None, None])
    parser_a.set_defaults(func=remap_bt)

    # Subparser for querying a beheertype
    parser_b = subparsers.add_parser('query4BT', help='Get information on the abtiotic optimum for a Beheertype')
    parser_b.add_argument('bt', help='Beheertype code', type=str)
    parser_b.add_argument('abiotic', help='abiotic condition', type=str, choices=['gvg', 'ph', 'ndep'])
    parser_b.add_argument('--of', help='output format', type=str, choices=['tuple', 'df', 'dict', 'single', 'str'],
                          default='str')
    parser_b.add_argument('--sp_list', help='which species list', type=int, choices=[146, 281, 468], default=281)
    parser_b.set_defaults(func=query4bt)

    # Subparser for abiotic range of a Species
    parser_c = subparsers.add_parser('query4Species', help='get Abiotic limit values for a species')
    parser_c.add_argument('species', help='Species local name, scientific name or code.')
    parser_c.add_argument('abiotic', help='abiotic condition', type=str, choices=['gvg', 'ph', 'ndep'])
    parser_c.add_argument('--of', help='output format', type=str, choices=['dict', 'csv'],
                          default='csv')
    parser_c.set_defaults(func=query4sp)

    # Subparser for species response to a abiotic value
    parser_d = subparsers.add_parser('SingleSpeciesResponse', help='Get Species response to an abiotic value')
    parser_d.add_argument('species', help='Species local name, scientific name or code.')
    parser_d.add_argument('abiotic', help='abiotic condition', type=str, choices=['gvg', 'ph', 'ndep'])
    parser_d.add_argument('val', help='value of abiotic condition', type=float)
    parser_d.add_argument('--of', help='output value', type=str, choices=['simpleton', 'verbose', 'dict'],
                          default='simpleton')
    parser_d.set_defaults(func=species_response)

    # Subparser for species response to >1 abiotic value
    parser_e = subparsers.add_parser('MixedSpeciesResponse', help='Get species response to 1 or more abiotic value(s)')
    parser_e.add_argument('species', help='Species local name, scientific name or code.')
    parser_e.add_argument('abiotics', help='one or more abiotic - value pairs', nargs='*')
    parser_e.set_defaults(func=mixed_species_response)


    try:
        args = parser.parse_args()
        args.func(**vars(args))
    except AssertionError as e:
        print('\nError! {0}.'.format(e))
        sys.exit(1)


