import rasterio as rio
from rasterio.plot import show
import pandas as pd
import geopandas as gp
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from mpl_toolkits.axes_grid1 import make_axes_locatable
import os


def mnp_hokaggregation(hok_csv, hokken_polygon_path, background_raster_path, outputdir, gridcell_count=False):
    """
    make aggregationplots for use in manual validation of mnp runs. can both aggregate validation data and species
    model gridcells per hok.
    :param hok_csv: csv file with value per hok
    :param hokken_polygon_path: path to hokken polygon file
    :param background_raster_path: path to raster file to be used as background. raster should only contain on value
     lower than or equal to 0, and 1's.
    :param outputdir: directory to save plot
    :return:
    """

    # load hokken
    hokken = gp.read_file(hokken_polygon_path)
    if "ID" not in hokken.columns:
        raise KeyError("No hok identifiercolumn called ID column in shapefile")

    # load and prepare background raster
    rast = rio.open(background_raster_path)
    background = rast.read(1)
    background[background < 0] = 0
    cmp = colors.ListedColormap([[0.97, 0.97, 0.97, 1], [0.8, 0.8, 0.8, 1]])

    # Load counts data and shapes
    counts = pd.read_csv(hok_csv)
    colnames = list(counts.columns)
    colnames[colnames.index("UH_ID")] = "ID"
    counts.columns = colnames
    counthokken = pd.merge(hokken, counts, on="ID")

    if "MEAN" in colnames:
        bounds = np.round(np.arange(1,counts.MEAN.max(),counts.MEAN.max()/6),1)
        explabel = "_"
        valuecolumn = "MEAN"
        extend = "max"
        ylabel_text = "Number of observations in SOVON"

    elif "OPS_COUNT" in colnames:
        bounds = np.array([1, 10, 50, 100, 500, 1000])
        explabel = "_validation"
        valuecolumn = "OPS_COUNT"
        extend = "max"
        ylabel_text = "number of observations in NDFF"

    elif gridcell_count:
        bounds = np.array([1, 10, 100, 1000, 10000, 40000])
        explabel = "_"
        valuecolumn = "COUNT_KEY_VALUE"
        extend = "neither"
        ylabel_text = "Number of gridcells in Hok"

    elif "MAX_KEY_VALUE" in colnames:
        bounds = np.array([0, 0.1, 0.5, 1, 1.5, 2])
        explabel = "_"
        valuecolumn = "MAX_KEY_VALUE"
        extend = "max"
        ylabel_text = "Largest key population in cell"

    else:
        raise Exception("no value column in columns")

    # Make the figure and plot, you know the drill
    plt.ioff()
    fig, ax = plt.subplots(figsize=(15, 15))
    plt.axis("off")
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size=0.25, pad=0.1)
    show(background, transform=rast.transform, ax=ax, cmap=cmp)
    counthokken.plot(
        ax=ax,
        alpha=0.8,
        column=valuecolumn,
        cmap="YlOrRd",
        edgecolor="darkgray",
        norm=colors.BoundaryNorm(boundaries=bounds, ncolors=256, extend=extend),
        linewidth=0.8,
        legend=True,
        cax=cax,
    )
    # cax is a seperate axes dedicated tot the colorbar legend on the right
    cax.set_ylabel(ylabel_text, size=20, fontweight="semibold")
    cax.tick_params(labelsize=15)

    # make outputdir if not exists and save figure
    outputdir = outputdir
    if not os.path.exists(outputdir):
        os.makedirs(outputdir)
    plt.savefig(
        outputdir + "/" + os.path.basename(hok_csv)[:-4] + ".png", bbox_inches="tight", dpi=400
    )
    fig.clear()
    plt.close(fig)
