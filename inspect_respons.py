import os.path
import pandas as pd
import pathlib


class ResponseFile:

    def __init__(self, src, sep=',', comment='#', species_column='species_code'):
        self.src = src
        self.df = pd.read_csv(src, sep=sep, comment=comment)
        self.species_column = species_column
        assert self.df.shape[0] == len(set(getattr(self.df, species_column)))

        self.code_to_groupname = {
            'S02': 'S02 vogel',
            'S06': 'S06 vlinder',
            'S09': 'S09 plant'
        }

    def print_src(self):
        print(f'Analysing {os.path.join(os.path.basename(os.path.dirname(self.src)), os.path.basename(self.src))}')

    def n_species(self):
        self.print_src()
        print(f'{self.df.shape[0]} species.')

    def n_species_group(self, of='print'):
        counts = getattr(self.df, self.species_column).str[:3].map(self.code_to_groupname).value_counts()
        counts['total'] = counts.sum()

        if of == 'print':
            self.print_src()
            print(counts.sort_index())
        else:
            return counts

    def u_species(self):
        return list(getattr(self.df, self.species_column).unique())


if __name__ == '__main__':
    import argparse

    argumentparser = argparse.ArgumentParser()
    argumentparser.add_argument('src', help='source file', type=str)
    argumentparser.add_argument('--n', action='store_true', help='total species count.')
    argumentparser.add_argument('--n_group', action='store_true', help='count per group.')
    argumentparser.add_argument('--sep', default=',', help='column seperator')
    argumentparser.add_argument('--comment', default='#', help='comment char.')
    argumentparser.add_argument('--id_col', default='species_code', help='species ID column.')

    args = argumentparser.parse_args()

    response_file = ResponseFile(src=args.src,
                                 sep=args.sep,
                                 comment=args.comment,
                                 species_column=args.id_col)

    if args.n:
        response_file.n_species()

    elif args.n_group:
        response_file.n_species_group()









