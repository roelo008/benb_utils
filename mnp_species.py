"""
Module to read MNP species CSV tab and provide several helper dictionaries
"""

import pandas as pd
import os
import pathlib

filepath = pathlib.Path(__file__)

# De volledige Species tabel als Pandas dataframe
species_tab = pd.read_csv(os.path.join(os.path.dirname(filepath), r'resources\mnp_species.csv'), sep=',', comment='#')

# Dictionaries tussen code, lokale naam, wetenschappelijke naam
code2local = dict(zip(species_tab.Species_code, species_tab.Local_name))
local2code = dict(zip(species_tab.Local_name, species_tab.Species_code))

code2scientific = dict(zip(species_tab.Species_code, species_tab.Scientific_name))
scientific2code = dict(zip(species_tab.Scientific_name, species_tab.Species_code))

local2scientific = dict(zip(species_tab.Local_name, species_tab.Scientific_name))
scientific2local = dict(zip(species_tab.Scientific_name, species_tab.Local_name))

code2taxon = dict(zip(species_tab.Species_code, species_tab.taxon))

# Dictionaries to self, is usefull sometimes. Trust me...
local2local = dict(zip(species_tab.Local_name, species_tab.Local_name))
code2code = dict(zip(species_tab.Species_code, species_tab.Species_code))
scientific2scientific = dict(zip(species_tab.Scientific_name, species_tab.Scientific_name))

# Dict between species code and boolean species list
code2sel468 = dict(zip(species_tab.Species_code, species_tab.sel468.map({1: True,  0: False})))
code2sel281 = dict(zip(species_tab.Species_code, species_tab.sel281.map({1: True,  0: False})))
code2sel146 = dict(zip(species_tab.Species_code, species_tab.sel146.map({1: True,  0: False})))
code2selall = dict(zip(species_tab.Species_code, species_tab.selall.map({1: True,  0: False})))
