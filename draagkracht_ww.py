import numpy as np


def draagkracht_ww(frequentie, trouw, th, tuning):
    """
    Draagkracht formule volgens Wieger Wamelink. Zie MS Teams discussie 16-07-2020 MNP2020/Fase 7 Objectivering Draagk..
    :param frequentie: gemiddelde frequentie van soort X binnen beheertype Y
    :param trouw: gemiddelde trouw van soort X binnen beheertype Y
    :param th: minumum draagkracht value
    :param tuning: subjective tuning factor for achieving best results
    :return: draagkracht soort X - beheertype Y combinatie, gemaximaliseerd tot 1.
    """
    dk = np.min([np.multiply(np.divide(np.max([frequentie, trouw]), 100), tuning), 1])
    if dk >= th:
        return dk
    else:
        return np.nan