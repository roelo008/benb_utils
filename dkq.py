"""
python program for quering MNP draagkracht file
Hans Roelofsen, jan 2022
"""

import sys
import pandas as pd
import os
import numpy as np

from fix_bt import fix_bt  # imports a function
from snl_beheertypen import get_snl_beheertypen_list  # imports a function
from snl_beheertypen import code2name  # imports a dictionary
from Classes import Species as species  # imports a module with two classes

species_table = species.TabularSpecies()


class DraagKracht:

    def __init__(self, src: str = r'w:\PROJECTS\QMAR\MNP-SNL-ParameterSet\Parameters_v05_2021_04_08\03_MNP_versie5_par_density_factors_BT2021_v3.csv',
                 bt_column: str = 'Land_type_code',
                 id_column: str = 'Species_code',
                 quality_column: str = 'Land_type_quality'):
        self.src = src
        self.snl_bt = get_snl_beheertypen_list(of="full")
        self.use_default = True
        self.bt_column = bt_column
        self.id_column = id_column
        self.quality_column = quality_column
        self.df = pd.read_csv(src)

        self.code_to_groupname = {
            "S02": "S02 vogel",
            "S06": "S06 vlinder",
            "S09": "S09 plant",
        }

    def print_src(self):
        print(
            f"Analysing {os.path.join(os.path.basename(os.path.dirname(self.src)), os.path.basename(self.src))}"
        )

    def query4bt(self, bt_lst: list, of="brief", sp_sel="281"):
        """
        query the draagkrachten for a specific beheertype
        :param bt_lst: beheertype code
        :param of: output format
        :param sp_sel: use which species list? 1018, 468 or 281?
        :return: str to stdout
        """

        if isinstance(bt_lst, str):
            bt_lst = [bt_lst]

        for bt in bt_lst:
            query = "({0} in {1}) and selection == True".format(
                self.bt_column, [fix_bt(bt, as_mnp=True)]
            )
            sel = self.df.assign(
                selection=self.df.loc[:, self.id_column].map(
                    getattr(species_table, "code2sel{}".format(sp_sel))
                )
            ).query(query)
            counts = sel.loc[:, self.id_column].map(species_table.code2taxon).value_counts()

            message_brief = "{0}-{1}: {2} vogel, {3} vlinder {4} plant. {5} total (out of {6}).".format(
                bt,
                code2name[fix_bt(bt, as_mnp=True)],
                counts.get("V", 0),
                counts.get("E", 0),
                counts.get("P", 0),
                sel.shape[0],
                sp_sel if sp_sel != "all" else "1081",
            )
            df_out = (
                sel.assign(
                    local=sel.loc[:, self.id_column].map(species_table.code2local),
                    taxon=sel.loc[:, self.id_column].map(species_table.code2taxon),
                    latin=sel.loc[:, self.id_column].map(species_table.code2scientific),
                    lst468=sel.loc[:, self.id_column].map(
                        getattr(species_table, "code2sel468")
                    ).multiply(1),
                    lst281=sel.loc[:, self.id_column].map(
                        getattr(species_table, "code2sel281")
                    ).multiply(1),
                    lst146=sel.loc[:, self.id_column].map(
                        getattr(species_table, "code2sel146")
                    ).multiply(1),
                )
                .sort_values(by=["taxon", "local"])
                .loc[
                    :,
                    [
                        self.id_column,
                        "taxon",
                        self.quality_column,
                        "local",
                        "latin",
                        "lst468",
                        "lst281",
                        "lst146",
                    ],
                ]
            )
            message_full = df_out.to_csv(sep="\t", index=False, header=True)
            dict_out = dict(zip(sel[self.id_column], sel.loc[:, self.quality_column]))

            if of == "brief":
                print(message_brief)
            elif of == "full":
                print(message_brief)
                print(message_full)
                df_out.to_clipboard(index=False, sep="\t")
            elif of == "df":
                return df_out
            elif of == "dict":
                return dict_out

    def query4species(self, species_lst: list, of, verbose=False):
        """
        query draagkrachten for one or more species
        :param species_lst:
        :param of:
        :return:
        """

        if not isinstance(species_lst, list):
            species_lst = [species_lst]

        for x in species_lst:
            sp = species.IndividualSpecies(x)
            query = 'Species_code in ["{0}"]'.format(sp.code)
            sel = self.df.query(query)
            if verbose:
                print(
                    "{0} ({1}-{2}. Listed in: {3}): {4} beheertypen".format(
                        sp.local, sp.scientific, sp.code, sp.groupinfo, sel.shape[0]
                    )
                )

            df = (
                sel.assign(desc=getattr(sel, self.bt_column).map(code2name))
                .loc[:, ["Land_type_quality", self.bt_column, "desc"]]
                .sort_values(by=self.bt_column)
            )

            if of == "full":
                df.to_clipboard(sep="\t", index=False)
                print(df.to_csv(sep="\t", index=False, header=False))
            elif of == "class":
                for row in df.itertuples():
                    setattr(sp, getattr(row, self.bt_column), row.Land_type_quality)
                return sp

    def u_species(self):
        return list(getattr(self.df, self.id_column).unique())

    def n_species(self, of="print"):
        u_species = pd.Series(self.u_species())
        counts = (
            u_species.str[:3]
            .map(self.code_to_groupname)
            .value_counts()
            .loc[[x for x in self.code_to_groupname.values()]]
        )
        if of == "print":
            self.print_src()
            print(counts.loc[list(self.code_to_groupname.values())])
        else:
            return counts

    def n_landscapes(
        self, of="print"):

        self.df["hoofd"] = np.where(
            self.df.loc[:, self.bt_column].str[-2:] != "00",
            "N-neer",
            self.df.loc[:, self.bt_column].str[:1],
        )

        p1 = pd.pivot_table(
            self.df, index="hoofd", values=self.bt_column, aggfunc=lambda x: len(x.unique())
        )
        p1 = p1.join(
            other=pd.pivot_table(
                self.df,
                index="hoofd",
                values=self.id_column,
                aggfunc=lambda x: len(x.unique()),
            )
        ).rename(columns={self.bt_column: 'n landscapes', self.id_column: 'n species'})

        if of == "print":
            print(p1.sort_index())
        else:
            return p1.sort_index()

    def summary(self):
        self.print_src()
        print(
            f"\n==Summary==\n{self.df.shape[0]:4d} combinations\n{len(self.u_species()):4d} species\n{len(self.df.loc[:, self.bt_column].unique()):4d} landscapes\n"
        )
        print('==Species overview==')
        print(self.n_species(of="other").to_csv(sep='\t', header=False))
        print('==Landscape overview==')
        print(self.n_landscapes(of="other"))


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        "src",
        type=str,
        help="Source file",
        default="w:\PROJECTS\QMAR\mnp_rebuild_2022\tabs\land_type_suitability_index.csv",
    )
    parser.add_argument("--bt_col", type=str, default=r"Land_type_code", help='type code column name')
    parser.add_argument("--id_col", type=str, default=r"Species_code", help='species code column name')
    parser.add_argument("--quality_col", type=str, default=r"Land_type_quality", help='land type quality column name')
    parser.add_argument("--qbt", nargs="+", type=str)
    parser.add_argument("--qsp", nargs="+", type=str)
    parser.add_argument(
        "--of", choices=["full", "sparse", "df", "dict"], default="full", help='output format'
    )
    parser.add_argument(
        "--sp_list",
        choices=["468", "281", "146", "NSensitive", "all"],
        default="281",
        type=str,
        help='species list'
    )
    parser.add_argument("--n_species", help="Return species count", action="store_true")
    parser.add_argument(
        "--n_landscapes", help="Return landscape count", action="store_true"
    )
    parser.add_argument("--summary", help="Print content summary", action="store_true")
    args = parser.parse_args()

    draagkracht_tabel = DraagKracht(
        src=args.src, bt_column=args.bt_col, id_column=args.id_col,
        quality_column=args.quality_col
    )

    try:
        if args.qbt:
            draagkracht_tabel.query4bt(bt_lst=args.qbt, of=args.of, sp_sel=args.sp_list)
        elif args.qsp:
            draagkracht_tabel.query4species(species_lst=args.qsp, of=args.of)
        elif args.n_species:
            draagkracht_tabel.n_species()
        elif args.summary:
            draagkracht_tabel.summary()
            # print(draagkracht_tabel.n_species(of='other'))
    except AssertionError as e:
        print(e)
        sys.exit(1)


#
# d = {'S02': 'V',
#      'S06': 'E',
#      'S09': 'P'}
# full = pd.read_csv(r'w:\PROJECTS\QMAR\MNP-SNL-ParameterSet\Parameters_v06_2019_12_09\01_Base_species_name.csv', sep=',', comment='#')
# full.set_index(keys='Species_code', drop=False, inplace=True)
# full['taxon'] = full.index.str.slice(0,3).map(d)
# sel468 = pd.read_csv(r'W:\PROJECTS\QMAR\MNP-SNL-ParameterSet\Parameters_v05_2021_04_08\09_MNP_versie4_Group_Species_valid model_468.csv', sep=',', index_col='Species_code')
# sel281 = pd.read_csv(r'W:\PROJECTS\QMAR\MNP-SNL-ParameterSet\Parameters_v05_2021_04_08\09_MNP_versie4_Species_group_valid_model_280.csv', sep=',', index_col='Species_code')
# sel146 = pd.read_csv(r'W:\PROJECTS\QMAR\MNP-SNL-ParameterSet\Parameters_v06_2019_12_09\09_MNP_versie4_Species_group_valid_model_typisch_146.csv', sep=',', index_col='Species_code')
# in468 = full.index.intersection(sel468.index)
# in281 = full.index.intersection(sel281.index)
# in146 = full.index.intersection(sel146.index)
# full.loc[:, 'sel468'] = 0
# full.loc[in468, 'sel468'] = 1
# full.loc[:, 'sel281'] = 0
# full.loc[in281, 'sel281'] = 1
# full.loc[:, 'sel146'] = 0
# full.loc[in146, 'sel146'] = 1
# full.loc[:, 'sel1018'] = 1
# full.loc[:, 'selall'] = 1
# full.to_clipboard(sep=',', index=False)
