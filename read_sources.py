import os
import pandas as pd
import geopandas as gp
import sys


def read_gis(dir, lyr):
    """
    Read either shapefile from disk or layer from Geopacakge
    :param dir: file directory or GeoPackage
    :param lyr: shapefile name within file dir or GPKG layer name
    :return: geopandas geodataframe
    """
    try:
        if dir.endswith('gpkg'):
            params = {'filename': dir, 'layer': lyr}
        else:
            params = {'filename': os.path.join(dir, lyr)}
        foo = gp.read_file(**params)
        # add hectare
        foo['area_ha'] = foo.area/10000
        return foo
    except ValueError as e:
        print('Incorrect layer source or name: '.format(e))
        raise


def read_src(src, **kwargs):
    """Extract relevant lambda function to read a file and execute"""
    return {
        '.csv': lambda inner_src, **inner_kwargs: pd.read_csv(inner_src, inner_kwargs.get('sep')),
        '.dbf': lambda inner_src: gp.read_file(inner_src),
        '.xlsx': lambda inner_src, **inner_kwargs: pd.read_excel(inner_src, sheet_name=inner_kwargs.get('sheet'))
    }.get(os.path.splitext(src)[1], lambda x: None)(src, **kwargs)


def mapping_from_file(src: str, key_col: str, value_col: str, strict:bool=False, **kwargs) -> dict:
    """
    Read table from file and return dictionary between two of its columns
    :param src: path to the soruce file
    :param key_col: Key column
    :param value_col: Value column
    :param strict: exit system or not
    :kwargs sheet: sheet name for Excel source files
    :kwargs sep: seperator for csv source files
    :return: dictionary between k and v
    """

    # Verify input
    assert os.path.isfile(src), "{} is not a file".format(src)

    # Read source file
    try:
        df = read_src(src, **kwargs)
        assert df is not None, "invalid source file {}".format(src)
    except (PermissionError, FileNotFoundError, AssertionError) as e:
        print(e)
        raise

    # Get keys and values. Exceptions not caught
    keys, values = df[key_col], df[value_col]

    # Cleaning
    z = zip()

    # Verify keys are unique
    if len(set(keys)) != len(keys):
        counts = keys.value_counts()
        more_than_once = counts.loc[counts > 1].index
        msg = 'There are {0} unique keys, but the key column "{1}" has {2} items. Check for {3}'.format(len(set(keys)), key_col,
                                                                                         len(keys), more_than_once)
        raise AssertionError(msg)
    # Return dictionary
    return dict(zip(keys, values))
