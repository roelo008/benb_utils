# Data Management in team B&B  


door: Hans Roelofsen  
datum: 31 mei 20201
---
  
  
## Data mangement?
> _"Looking after you data to help your future self and your peers find, open, understand and reuse data"_   
> Shauna Ni Fhlaithearta

![figuur 1](images/DM1b.jpg "Data management yeah!")

Data management is een investering in het heden om het jezelf en je collegas in de toekomst makkelijker te maken. 

---
### Wat bedoelen we met _data_?
> _"een unieke verzameling gegevens die bewust en doelmatig verzameld, gecreeerd of ontworpen zijn en betrekking hebben op ons werkveld"_  
> eigen definitie

#### Bijvoorbeeld:
  * vegetatieopname
  * veldwaarnemingen
  * meetgegevens
  * interview-geluidsopname
  * enquête responsies
  * GIS kaarten
  * Tabellen

#### Maar niet (altijd): 
  * rapportages & publicaties
  * tussen-producten
  * Powerpoints
  * figuren
  * posters
 
#### Verschillende smaken data
  * externe data-providers ([CBS](https://opendata.cbs.nl/statline/), [NGR](https://www.nationaalgeoregister.nl/geonetwork/srv/dut/catalog.search#/home))
  * project-partners (PBL, SBB, etc)  
  * intern
    * tijdelijk/probeersels
    * tussenproducten
    * data van eerdere projecten
    * data van collega projecten   
    * project-eindresultaat 

---    
### Project Data Cyclus
![Cyclus](images/DM2.jpg)  
Simpel gezegd: een project verzameld en analyseert data, wat resulteert in publicaties en een of meerdere `Project-Resultaat` datasets (dwz _datasets die opgeleverd worden door het project, die behouden moeten worden voor toekomstige referentie of gebruik_).  

Goed uitgevoerd data-management garandeert dat: 

  1. de resultaten uit het rapport gereproduceerd kunnen worden 
  2. de project-data kunnen gebruikt worden in een toekomstig project. 


---
### 1. Data vinden
![Sherlock](images/DM3c.jpg)  
Om data te kunnen vinden moet het goed opgeslagen zijn. 
```
goed opgeslagen data == vindbaar
vindbare data == goed opgeslagen
```
Wat zijn geschkte locaties voor data-opslag voor een lopend WEnR project?
  * project-directory op `W:\projects`
    * toegankelijk binnen WUR netwerk
    * aanvragen bij Servicedesk-IT
    * niet gratis, wel veilig
    * geschikt voor _grote_ bestanden
    * bijvoorbeeld: 
        * `W:/projects/QMAR`
        * `W:/projects/nvk_bdb`
        * `W:/projects/doren2019`
          
![W request](images/DM9.JPG)

  * MS OneDrive  
    ![OneDrive](images/DM10.jpg)
    * standaard bij WUR account 
    * persoonsgebonden, maar deelbaar met collegas (ook buiten WUR netwerk)
    * ook offline beschikbaar (sync zodra je online bent
  * MS Sharepoint  
![Sharepoint](images/DM11.jpg)    
      * vooral bedoeld voor documenten, minder geschikt voor grote data
      * document-management systeem
      * draait achter Teams
      * Sync met Windows Explorer
  * lokaal (`C:\\`)
    * voor lokale werkzaamheden, probeersels, tussenproducten
    * niet deelbaar, niet toegankelijk
  * partner oplossingen
    * Provincie Zeeland Fileshare
    * anders?  
    
Wat zijn geschikte locaties voor langdurige opslag van `Project-Resultaat` datasets?      
  * wederom `W:\\`
  * externe HardDisk (wel goed opbergen)
  * papier, ordner (formulieren, metingen etc)
  * [4TU Data Archief](https://data.4tu.nl/info/)  
![4TU](images/DM12.jpg)    
  * bij project-partners  
  * domeinspecifiek:
    * SynBioSys (Vegetatieopnamen)
    * GeoDesk (GIS kaarten)
    * anders
    
_Maar altijd!_ 

  * Goede datasets zijn:
    * **compleet**
    * **intern homogeen**
    * **correct**
  * Organiseer data op een logische manier. Bijvoorbeeld: 
    * chronologisch 
    * herkomst
    * geografisch
    * thematisch
    * alfabetisch
  * losse bestanden alleen in laagste folder-niveau  
  * forceer de volgorde van directories in file-system mbv prefixes  
![Tree](images/DM4.JPG)

    
---
### 2. Data begrijpen
![bright light](images/DM5.jpg)  
Data moet beschreven worden om begrepen te worden. `Metadata` is het toverwoord. 

**Hoe leg ik metadata vast?**
  * apart tabblad in Excel
  * los bestandje `ReadMe.txt`
  * commentaar regels bovenaan tabel, ```# Tabel gemaakt door HR, dd 20210528```  
  * e-mail correspondentie
  * MS Office document Properties  
![properties](images/DM6.JPG)


**Wat te vermelden?**
  * meeteenheden (cm, m, hectare, km<sup>2</sup>)
  * legendas van kaarten
  * data-schaal (ordinaal, nominaal, interval)
  * afkortingen
  * kolom-namen
  * referentiedata (coordinaten-systeem, NAP)
  * datum-formatting ([ISO8601](https://en.wikipedia.org/wiki/ISO_8601) zegt: `YYYY-MM-DD`)
  * auteurs + datum
  * gebaseerd op welke data?
  * wanneer gemaakt, wanneer gewijzigd?
  * gemaakt voor welk project? 
  * gebruiksbeperkingen (copyright, vertrouwelijheid)

![shouting](images/DM7.jpg)

**Voor metadata geldt:**

> iets > niets  
> expliciet > impliciet  
> te veel > te weinig

---
### 3. Data gebruiken

Als we _nu_ op onze data passen, maken we het onze _toekomstige_ zelf en collega's gemakkelijker. Dankzij data-management:

* is onderzoek gebaseerd op betrouwbare, herleidbare data
* is onderzoek reproduceerbaar
* kan data meermaals gebruikt worden

![I can do science!](images/DM8.jpg)

---  

### Meer weten?

* [Wageningen Data Competence Centre](https://www.wur.nl/en/Value-Creation-Cooperation/Collaborating-with-WUR-1/WDCC.htm)
  * [WDCC FAQ](https://www.wur.nl/en/Value-Creation-Cooperation/Collaborating-with-WUR-1/WDCC/FAQ-Wageningen-Data-Competence-Center.htm)
  * [WDCC Data Management Plants](https://www.wur.nl/en/Value-Creation-Cooperation/Collaborating-with-WUR-1/WDCC/Data-Management-WDCC/Planning/Data-Management-Plans_templates_examples.htm)
  * [WDCC Data Storage](https://www.wur.nl/en/Value-Creation-Cooperation/Collaborating-with-WUR-1/WDCC/Data-Management-WDCC/Doing/Storage-solutions.htm)
  * [WDCC Data Archiving](https://www.wur.nl/en/Value-Creation-Cooperation/Collaborating-with-WUR-1/WDCC/Data-Management-WDCC/Data-policy/Archiving.htm)
  * [WDCC Data Registration](https://www.wur.nl/en/Value-Creation-Cooperation/Collaborating-with-WUR-1/WDCC/Data-Management-WDCC/Data-policy/Registering.htm)
  * [WDCC Data Ownership](https://www.wur.nl/en/Value-Creation-Cooperation/Collaborating-with-WUR-1/WDCC/Data-Management-WDCC/Data-policy/Data-ownership-Policy.htm)
  * [WDCC Data Sharing](https://www.wur.nl/en/Value-Creation-Cooperation/Collaborating-with-WUR-1/WDCC/Data-Management-WDCC/Data-policy/Data-Sharing-guidelines.htm)
* [Library Support](https://www.wur.nl/en/Library/Researchers/Library-support.htm)  
* [Servicedesk IT File Storage FAQ](https://intranet.wur.nl/umbraco/en/practical-information/it-services/file-storage/)
* [Servicedesk IT Document Management FAQ](https://intranet.wur.nl/umbraco/en/practical-information/it-services/file-storage/)
* [Servicedesk IT request W:\ storage](https://iwms.wur.nl/case/IT/IT_PSS046?2)
* [HOW TO: Add users to W:\ share](https://sharepoint.wur.nl/sites/it/dataopslag_storage/EN-Manual_Edit_accesslist_networkshare.pdf)
* [Servidedesk IT Data Storage page](https://itsupport.wur.nl/CategorySubjects?selectedCategoryId=7)
* [Servidedesk IT Collaboration page](https://itsupport.wur.nl/CategorySubjects?selectedCategoryId=15)
* [FAIR Data @ WUR](https://www.wur.nl/nl/nieuws/WUR-stappen-naar-FAIR-data-science.htm)
* ESG Data Stewards Core Team: 
   * Wies Vullings 
   * Maarten Storm
   * Paul de Bie
   * Janien van der Greft 
* team B&B Data Management plan: volgt
* team B&B Data Steward: Hans




 

    

    
    








  

