"""
pandas dataframe containing .\resources\snl_beheertypen.csv
Hans Roelofsen, 14/07/2021
"""

import os
import pathlib
import pandas as pd
import sys
# this only works if the dir containing benb_utils has already been appended to sys.path
try:
    from benb_utils import fix_bt as fbt
except ModuleNotFoundError:
    import fix_bt as fbt

filepath = pathlib.Path(__file__)
benb_dir = os.path.dirname(filepath)
csv_file = os.path.join(benb_dir, 'resources/snl_beheertypen.csv')


def get_snl_beheertypen_list(of='sparse'):
    """
    return snl beheertypen list as pandas df
    :param benb_dir: directory of benb_utils
    :return: pandas dataframe
    """

    csv_target = os.path.join(pathlib.Path(__file__).parents[0],
                              'resources', 'snl_beheertypen.csv')
    df = pd.read_csv(r'https://git.wur.nl/roelo008/benb_utils/-/raw/master/resources/snl_beheertypen.csv',
                     sep=',', comment='#', quotechar='"')
    mnp_codes = df.land_type_code.apply(fbt.fix_bt, as_mnp=True)
    return {'full': df.assign(mnp_code=mnp_codes),
            'sparse': df}[of]

df = pd.read_csv(csv_file, sep=',', comment='#', quotechar='"')

code2name = dict(zip(df.land_type_code.apply(fbt.fix_bt, as_mnp=True),
                            df.land_type_name))
                            
code2code_name = dict(zip(df.land_type_code.apply(fbt.fix_bt, as_mnp=True),
                          df.land_type_code_name))

# TODO: hier moet eigenlijk een functie komen als:
# def code2anythin(code)
# code --> code as mnp
# code --> code as SNL
# code --> description
# code --> code_description
# waarbij t niet uitmaakt of de code als MNP is, of niet!

def btcode2description(code):
    # Note: dit hoeft niet perse in een functie te staan.
    try:
        return dict(zip(df.land_type_code.apply(fbt.fix_bt, as_mnp=True),
                        df.land_type_name))[fbt.fix_bt(code, as_mnp=True)]
    except KeyError as e:
        print('\n{} is not a valid beheertype\n'.format(code))
        sys.exit(0)


